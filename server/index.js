const express = require("express");
const ejs = require("ejs");
const morgan = require("morgan");
const cors = require("cors");
const bodyparser = require("body-parser");

const app = express();
import db from "./db";
app.use(morgan("dev"));
app.use(cors());
app.use(bodyparser.json());
app.get("/", (req, res) => {
  let data = {
    title: "Home",
    nama: "Bahri"
  };
  ejs.renderFile("view/view.html", data, (err, str) => {
    if (err) throw err;
    res.send(str).end();
  });
});

app.get("/panggil/:nama", (req, res) => {
  let data = {
    title: "Panggil " + req.params.nama,
    nama: req.params.nama
  };
  ejs.renderFile("view/view.html", data, (err, str) => {
    if (err) throw err;
    res.send(str).end();
  });
});

app.get("/siswa", async (req, res) => {
  let data = await db.query("SELECT * FROM tbl_manusia", {
    type: db.QueryTypes.SELECT
  });
  try {
    res.json(data);
  } catch (err) {
    res.status(500);
    throw err;
  }
});

app.post("/siswa/input", async (req, res) => {
  try {
    const { nama, jk, alamat } = req.body;
    console.log(req.body);
    const input = await db.query(
      `INSERT INTO tbl_manusia VALUES('', '${nama}', '${jk}', '${alamat}')`,
      {
        type: db.QueryTypes.INSERT
      }
    );
    res.json(input);
  } catch (err) {
    res.status(500);
    throw err;
  }
});

app.post("/siswa/delete", async (req, res) => {
  try {
    const id = req.body.id;
    db.query(`DELETE FROM tbl_manusia WHERE id_manusia = ${id}`, {
      type: db.QueryTypes.DELETE
    });
  } catch (err) {
    console.log(err);
    res.status(500);
  }
});

app.put("/siswa/update", async (req, res) => {
  try {
    const { id, nama, jk, alamat } = req.body;
    const input = await db.query(
      `UPDATE tbl_manusia SET nama = '${nama}', jk = '${jk}', alamat = '${alamat}' WHERE id_manusia = ${id}`,
      {
        type: db.QueryTypes.UPDATE
      }
    );
  } catch (err) {
    console.log(err);
    res.status(500);
  }
});
const port = 7007;
app.listen(port, () => {
  console.log("Server started on port " + port);
});
