const Sequelize = require("sequelize");

const db = new Sequelize("latihan", "root", "", {
  host: "localhost",
  dialect: "mysql"
});

const auth = () => {
  db.authenticate()
    .then(() => {
      console.log("DB Connected");
    })
    .catch(err => {
      console.log(err);
      setTimeout(() => {
        auth();
      }, 200);
    });
};

auth();

export default db;
