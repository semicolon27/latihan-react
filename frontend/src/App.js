import React, { Component } from "react";
import axios from "axios";
import logo from "./logo.svg";
import "bootstrap/dist/css/bootstrap.min.css";
import { NavbarComponent } from "./components/common/common";
import { Jumbotron, JumbotronComponent } from "./components/components";
import TabelComponent from "./components/Tabel";
import TableComponent from "./components/TableComponent";
import FormComponent from "./components/FormComponent";
import siswa from "./data/siswa";
import { Container, Button, Row, Col } from "reactstrap";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      siswa: [],
      data: [],
      edit: null
    };
  }

  componentDidMount() {
    this.getData();
    console.log(this.state.siswa);
  }

  getData = () => {
    axios.get("http://localhost:7007/siswa").then(data => {
      this.setState({ siswa: data.data });
    });
  };

  render() {
    return (
      <React.Fragment>
        <NavbarComponent />
        <Row>
          <Col md="4">
            <FormComponent
              onAdd={this.getData}
              onEdit={this.getData}
              editData={this.state.edit}
              onUpdate={data => {
                axios
                  .put("http://localhost:7007/siswa/update", { data })
                  .then(() => this.getData);
              }}
              selectEdit={data => {
                this.setState({ data: data });
              }}
            />
          </Col>

          <Col>
            <Container>
              <TableComponent
                title="Tabel Murid"
                data={this.state.siswa}
                selectEdit={data => {
                  this.setState({ edit: data });
                }}
                onDelete={id => {
                  axios
                    .post("http://localhost:7007/siswa/delete", { id })
                    .then(() => this.getData);
                }}
              />
            </Container>
          </Col>
        </Row>
        <Container>
          {/* <TabelComponent /> */}
          <Button onClick={this.getData}>Reload</Button>
        </Container>
      </React.Fragment>
    );
  }
}

export default App;
