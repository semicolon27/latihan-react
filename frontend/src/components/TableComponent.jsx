import React, { Component } from "react";
import { Table, Button } from "reactstrap";
import Axios from "axios";

export default function TableComponent(props) {
  const thead = ["No", "Nama", "Alamat", "Jenis Kelamin", "Aksi"];
  let no = 0;
  return (
    <>
      <h3>{props.title}</h3>
      <Table striped>
        <thead>
          <tr>
            {thead.map(v => {
              return <th>{v}</th>;
            })}
          </tr>
        </thead>
        <tbody>
          {props.data.map((v, i) => {
            no++;
            return (
              <tr>
                <td>{no}</td>
                <td>{v.nama}</td>
                <td>{v.alamat}</td>
                <td>{v.jk == 0 ? "Perempuan" : "Laki - Laki"}</td>
                <td>
                  <Button onClick={() => props.onDelete(v.id_manusia)}>
                    Hapus
                  </Button>
                  <Button onClick={e => props.selectEdit(v)}>Edit</Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </>
  );
}
