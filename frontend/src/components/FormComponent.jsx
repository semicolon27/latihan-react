import React, { Component } from "react";
import axios from "axios";
import { Row, Col } from "reactstrap";

class FormComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      nama: "",
      alamat: "",
      jk: "",
      edit: false
    };
  }

  componentWillReceiveProps(props) {
    if (props.editData != null) {
      this.setState({
        id: props.editData.id_manusia,
        nama: props.editData.nama,
        jk: props.editData.jk,
        alamat: props.editData.alamat,
        edit: true
      });
      console.log(props);
    }
  }

  handleNama = e => {
    this.setState({ nama: e.target.value });
  };
  handleAlamat = e => {
    this.setState({ alamat: e.target.value });
  };
  handleJK = e => {
    this.setState({ jk: e.target.value });
  };

  postData = e => {
    const form = {
      nama: this.state.nama,
      jk: this.state.jk,
      alamat: this.state.alamat
    };
    console.log(form);
    axios.post("http://localhost:7007/siswa/input", form).then(() => {
      this.props.onAdd();
    });
  };

  putData = e => {
    const form = {
      id: this.state.id,
      nama: this.state.nama,
      jk: this.state.jk,
      alamat: this.state.alamat
    };
    console.log(form);
    axios.put("http://localhost:7007/siswa/update", form).then(() => {
      this.props.onEdit();
    });
  };

  simpan = () => {
    this.props.onAdd({
      nama: this.state.nama,
      alamat: this.state.alamat,
      jk: this.state.jk
    });
  };
  render() {
    return (
      <div className="form-group container">
        <br />
        <Row>
          <Col xs="3">
            <label htmlFor="">Nama : </label>
          </Col>
          <Col auto>
            <input
              type="text"
              name="nama"
              id="nama"
              value={this.state.nama}
              className="form-control"
              onChange={this.handleNama}
            />
          </Col>
        </Row>
        <br />
        <Row>
          <Col xs="3">Alamat : </Col>
          <Col auto>
            <input
              type="text"
              value={this.state.alamat}
              name="alamat"
              className="form-control"
              onChange={this.handleAlamat}
            />
          </Col>
        </Row>
        <br />
        <Row>
          <Col xs="3">Jenis kelamin</Col>
          <Col auto>
            <select
              name="jk"
              id="jk"
              onChange={this.handleJK}
              className="form-control"
              value={this.state.jk}
            >
              <option value="" disabled>
                Jenis Kelamin
              </option>
              <option value="1">Laki - Laki</option>
              <option value="0">Perempuan</option>
            </select>
          </Col>
        </Row>
        <br />
        <button
          className="btn btn-success"
          onClick={this.state.edit == false ? this.postData : this.putData}
        >
          {this.state.edit == false ? "Simpan" : "edit"}
        </button>
        <br />
        <p>Nama saya : {this.state.nama}</p>
        <p>Alamat saya : {this.state.alamat}</p>
        <p>
          Jenis Kelamin Saya :
          {this.state.jk == ""
            ? ""
            : this.state.jk == 1
            ? "Laki - Laki"
            : "Perempuan"}
        </p>
      </div>
    );
  }
}

export default FormComponent;
